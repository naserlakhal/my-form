import React, { useState } from 'react';
import SongList from './components/SongLists';
import NewSongForm from './components/NewSong';
import { v4 as uuidv4 } from 'uuid';

function App() {
  const [songs, setSongs] = useState([
    { title: 'almost home', body:'parole almost home' ,userId: 1 },
    { title: 'memory gospel', body:'parole memory gospel' ,userId: 2 },
    { title: 'this wild darkness',body:'parole this wild darkness' , userId: 3 }
  ]);
  const addSong = (title,body) => {
    setSongs([...songs, { title,body, userId: uuidv4() }]);
  }
  return (
    <div className="song-list">
      <SongList songs= {songs}/>
      <NewSongForm addSong={addSong} />

    </div>
    
  );
}

export default App;
