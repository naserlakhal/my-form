import React, { useState,useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';

const NewSongForm = ({ addSong }) => {
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const [id] = useState(2);
    // useState can be used multiple times for different data
    const handleSubmit = (e) => {
        e.preventDefault();
        addSong(title, body);
        setTitle('');
        setBody('');
        // ! POST METHOD
        // fetch('https://jsonplaceholder.typicode.com/posts', {
        //     method: 'POST',
        //     body: JSON.stringify({
        //         title,
        //         body,
        //         userId: uuidv4(),
        //     }),
        //     headers: {
        //         'Content-type': 'application/json; charset=UTF-8',
        //     },
        // })
        //     .then((response) => response.json())
        //     .then((json) => console.log(json));
        // ! UPDATE METHOD

        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
            method: 'PUT',
            body: JSON.stringify({
                id,
                title,
                body,
                userId: uuidv4(),
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
            .then((response) => response.json())
            .then((json) => console.log(json));
    }
    return (
        <form onSubmit={handleSubmit}>
            <label>Song name:</label>
            <input type='text' value={title} onChange={(e) => setTitle(e.target.value)} />
            <label>Song body:</label>
            <input type='text' value={body} onChange={(e) => setBody(e.target.value)} />
            <input type='submit' value='add' />
        </form>
    );
}

export default NewSongForm;