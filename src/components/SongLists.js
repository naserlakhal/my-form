import React from 'react';

const SongList = (props) => {

	return (
		<>
			{props.songs.map((song, index) => (
                <>
                <li key={index}>{song.title}</li>
                <li key={index}>{song.body}</li>
                </>
				
			))}
		</>
	);
};

export default SongList;